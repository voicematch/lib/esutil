package event

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

const (
	// EmptyCorrelationID defines the default value of a correlation ID.
	EmptyCorrelationID = ""
)

// Send publishes an event to a kafka topic via a SyncProducer.
// If a cid (correlation ID) is provided, we use it to identify the
// event. If not, we automaticaly generate one.
// Event is serialized to JSON prior to sending to kafka.
// Correlation ID and creation date are automatically added to the
// message headers.
// If the correlation ID cannot be generated, an error is returned.
// If the event cannot be serialized or published to kafka, the
// correlation ID and an error are returned.
func Send(producer SyncProducer, topic, cid string, event interface{}) (string, error) {
	if cid == EmptyCorrelationID {
		id, err := uuid.NewUUID()
		if err != nil {
			return "", err
		}
		cid = id.String()
	}

	data, err := json.Marshal(event)
	if err != nil {
		return cid, err
	}

	meta := map[string]string{
		"correlation_id": cid,
		"created_at":     time.Now().UTC().Format(time.RFC3339),
	}

	_, _, err = producer.Send(topic, data, meta)
	return cid, err
}
