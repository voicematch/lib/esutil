package event

import (
	"github.com/Shopify/sarama"
)

// SyncProducer defines a generic Kafka synchronous message producer.
// Two methods are required: Send() which send the message to the broker
// with optinonal metadata, and Close() which stops the producer and closes
// the connection with the broker.
type SyncProducer interface {
	Send(topic string, value []byte, meta map[string]string) (int32, int64, error)
	Close() error
}

type saramaProducer struct {
	sarama.SyncProducer
}

// NewSaramaSyncProducer creates a new SyncProducer backed by sarama.SyncProducer.
func NewSaramaSyncProducer(brokers []string, cfg *sarama.Config) (SyncProducer, error) {
	producer, err := sarama.NewSyncProducer(brokers, cfg)
	if err != nil {
		return nil, err
	}
	return saramaProducer{producer}, nil
}

// Send publishes a message to the broker.
func (p saramaProducer) Send(topic string, value []byte, headers map[string]string) (int32, int64, error) {
	return p.SendMessage(&sarama.ProducerMessage{
		Topic:   topic,
		Value:   sarama.ByteEncoder(value),
		Headers: mapToHeaders(headers),
	})
}

func mapToHeaders(m map[string]string) []sarama.RecordHeader {
	var headers []sarama.RecordHeader

	for k, v := range m {
		headers = append(headers, sarama.RecordHeader{Key: []byte(k), Value: []byte(v)})
	}

	return headers
}
